# ECHOHEADERS
Based on the official Nginx image, it processes incoming requests and returns the associated headers using Lua.

##  How to build
To build this, you're going to need to set an argument like this:

```bash
docker image build --build-arg ENABLED_MODULES="ndk lua" -t mahyard/echoheaders:latest .
```
